# <img src="./web/images/favicon.png" width="24"> River RSS

River RSS is a simple self-hosted feed reader.

## Goals

- Simple, fairly minimal UI
- Usable in the browser's private window
- Easy enough to self-host
- Quick to open unread articles in new tabs, and automatically mark the articles as read

## Non-goals

- Article content view

## Build and run

River RSS consists of a backend and a frontend project. The following environment variables can be stored in a `.env` file.

```
DATABASE_URL=mysql://user:password@localhost/database
API_HOST=127.0.0.1:12000
CORS_ALLOW=http://127.0.0.1:1234
```

### Backend (Rust)

```sh
cd api

# Run
cargo run

# Build
cargo build
```

### Frontend (TypeScript)

```sh
cd web

# Run
parcel index.html

# Build
parcel build index.html
```

## License

The project is available under Mozilla Public License 2.0. See [`LICENSE`](./LICENSE) for details.
