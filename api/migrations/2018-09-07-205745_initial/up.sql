CREATE TABLE IF NOT EXISTS account (
  account_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  slug varchar(100) NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (account_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS feed (
  feed_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  account_id int(10) unsigned NOT NULL,
  url varchar(1000) NOT NULL,
  name varchar(100) NOT NULL,
  is_favorite tinyint(1) NOT NULL,
  last_fetch timestamp NULL DEFAULT NULL,
  last_fetch_error varchar(100) DEFAULT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (feed_id),
  KEY account_id (account_id),
  CONSTRAINT Feed_ibfk_1 FOREIGN KEY (account_id) REFERENCES account (account_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS article (
  article_id int(10) unsigned NOT NULL AUTO_INCREMENT,
  feed_id int(10) unsigned NOT NULL,
  title varchar(100) NOT NULL,
  url varchar(1000) NOT NULL,
  download_url varchar(1000) NULL,
  is_read tinyint(1) NOT NULL,
  published timestamp NULL DEFAULT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (article_id),
  KEY feed_id (feed_id),
  CONSTRAINT article_ibfk_1 FOREIGN KEY (feed_id) REFERENCES feed (feed_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO account (slug) VALUES ('example'), ('asdf'), ('abc');
INSERT INTO feed (account_id, url, name, is_favorite)
    VALUES
        (LAST_INSERT_ID(), "https://one.com", "first", 1),
        (LAST_INSERT_ID(), "https://two.org", "second", 0),
        (LAST_INSERT_ID(), "https://three.x", "third", 1),
        (LAST_INSERT_ID()+1, "https://four.se", "fourth", 0);
INSERT INTO article (feed_id, title, url, is_read)
    VALUES
        (LAST_INSERT_ID(), "Art A", "https://a", 1),
        (LAST_INSERT_ID(), "Art B", "https://b", 0),
        (LAST_INSERT_ID(), "Art C", "https://c", 1),
        (LAST_INSERT_ID()+1, "Art D", "https://d", 0);
