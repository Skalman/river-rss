#[macro_use]
extern crate diesel_migrations;
use river_rss::establish_connection;

embed_migrations!();

fn main() {
    let connection = establish_connection();

    embedded_migrations::run(&connection).unwrap();

    println!("Ran embedded migrations successfully");
}
