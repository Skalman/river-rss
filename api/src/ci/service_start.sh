#!/usr/bin/env bash

trap ctrl_c INT

function ctrl_c() {
  kill_silent
  exit
}

function kill_silent() {
  kill $PID > /dev/null 2>&1
}

while true; do
  ./migrate
  ./river-rss &

  # Get the previous command's process ID
  PID=$!

  inotifywait ./river-rss

  sleep 2
  kill_silent
  sleep 2
done
