use crate::fetch::FetchedFeed;
use crate::models::{Account, Article, Feed};
use crate::schema::account::dsl as account;
use crate::schema::article::dsl as article;
use crate::schema::feed::dsl as feed;
use diesel::dsl::{delete, insert_into, sql, update};
use diesel::mysql::MysqlConnection;
use diesel::prelude::*;
use diesel::result::Error::NotFound;
use diesel::sql_types::Bool;
use failure::{err_msg, Error};

fn expect_one(val: usize) -> Result<(), Error> {
    match val {
        1 => Ok(()),
        _ => Err(Error::from(NotFound)),
    }
}

pub fn get_account(conn: &MysqlConnection, slug: &str) -> Result<Account, Error> {
    let result = account::account
        .filter(account::slug.eq(slug))
        .first::<Account>(conn)?;

    Ok(result)
}

pub fn get_feeds(conn: &MysqlConnection, slug: &str) -> Result<Vec<FeedDto>, Error> {
    let result = account::account
        .inner_join(feed::feed.left_join(article::article))
        .filter(account::slug.eq(slug))
        // Diesel 1.3 doesn't really support group by.
        .group_by(feed::feed_id)
        .select((
            feed::feed::all_columns(),
            sql::<diesel::sql_types::Unsigned<diesel::sql_types::Integer>>(
                "count(article.article_id)",
            ),
            sql::<diesel::sql_types::Unsigned<diesel::sql_types::Integer>>(
                "count(case when article.is_read=0 then 1 end)",
            ),
        ))
        .load::<(Feed, u32, u32)>(conn)?;

    let result = result
        .iter()
        .map(|(feed, num_articles, num_unread_articles)| FeedDto {
            feed: feed.clone(),
            meta: FeedDtoMeta {
                num_articles: *num_articles,
                num_unread_articles: *num_unread_articles,
            },
        })
        .collect();

    Ok(result)
}

#[derive(Queryable, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FeedDtoMeta {
    num_articles: u32,
    num_unread_articles: u32,
}

#[derive(Queryable, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FeedDto {
    pub feed: Feed,
    pub meta: FeedDtoMeta,
}

pub fn get_feed(conn: &MysqlConnection, slug: &str, feed_id: u32) -> Result<FeedDto, Error> {
    let (feed, num_articles, num_unread_articles) = account::account
        .inner_join(feed::feed.left_join(article::article))
        .filter(account::slug.eq(slug))
        .filter(feed::feed_id.eq(feed_id))
        // Diesel 1.3 doesn't really support group by.
        .group_by(feed::feed_id)
        .select((
            feed::feed::all_columns(),
            sql::<diesel::sql_types::Unsigned<diesel::sql_types::Integer>>(
                "count(article.article_id)",
            ),
            sql::<diesel::sql_types::Unsigned<diesel::sql_types::Integer>>(
                "count(case when not article.is_read then 1 end)",
            ),
        ))
        .first::<(Feed, u32, u32)>(conn)?;

    Ok(FeedDto {
        feed,
        meta: FeedDtoMeta {
            num_articles,
            num_unread_articles,
        },
    })
}

fn get_feed_id_by_url(conn: &MysqlConnection, slug: &str, url: &str) -> Result<Option<u32>, Error> {
    let result = account::account
        .inner_join(feed::feed)
        .filter(account::slug.eq(slug))
        .filter(feed::url.eq(url))
        .select(feed::feed_id)
        .first::<u32>(conn);

    match result {
        Ok(feed_id) => Ok(Some(feed_id)),
        Err(NotFound) => Ok(None),
        Err(err) => Err(err.into()),
    }
}

fn get_account_id_by_feed_id(
    conn: &MysqlConnection,
    slug: &str,
    feed_id: u32,
) -> Result<u32, Error> {
    Ok(account::account
        .inner_join(feed::feed)
        .filter(account::slug.eq(slug))
        .filter(feed::feed_id.eq(feed_id))
        .select(account::account_id)
        .first::<u32>(conn)?)
}

fn get_account_id_by_slug(conn: &MysqlConnection, slug: &str) -> Result<Option<u32>, Error> {
    let result = account::account
        .filter(account::slug.eq(slug))
        .select(account::account_id)
        .first::<u32>(conn);

    match result {
        Ok(account_id) => Ok(Some(account_id)),
        Err(NotFound) => Ok(None),
        Err(err) => Err(err.into()),
    }
}

pub fn insert_feed(conn: &MysqlConnection, slug: &str, url: &str) -> Result<FeedDto, Error> {
    if !url.starts_with("https://") && !url.starts_with("http://") {
        return Err(err_msg("URLs must start with either http:// or https://"));
    }

    let now = chrono::Utc::now().naive_utc();

    let account_id = match get_account_id_by_slug(conn, slug)? {
        Some(account_id) => account_id,
        None => {
            // Create an account
            insert_into(account::account)
                .values(vec![Account {
                    account_id: 0,
                    slug: slug.to_string(),
                    created: now,
                }])
                .execute(conn)?;

            get_account_id_by_slug(conn, slug)?
                .ok_or_else(|| err_msg("Can't find the account that was supposedly inserted"))?
        }
    };

    if get_feed_id_by_url(&conn, slug, url)?.is_some() {
        return Err(err_msg("A feed with this URL already exists"));
    }

    insert_into(feed::feed)
        .values(Feed {
            feed_id: 0,
            account_id,
            url: url.to_string(),
            name: url.to_string(),
            is_favorite: true,
            last_fetch: None,
            last_fetch_error: None,
            created: now,
        })
        .execute(conn)?;

    let feed_id = get_feed_id_by_url(&conn, slug, url)?;

    match feed_id {
        Some(feed_id) => get_feed(&conn, slug, feed_id),
        None => Err(err_msg("Can't find the feed that was supposedly inserted")),
    }
}

pub fn update_feed(conn: &MysqlConnection, slug: &str, feed: &Feed) -> Result<(), Error> {
    // Diesel 1.3 doesn't support update with joined tables
    // https://github.com/diesel-rs/diesel/issues/1478

    get_feed(conn, slug, feed.feed_id)?;

    let result = update(feed::feed.find(feed.feed_id))
        .set(feed::is_favorite.eq(feed.is_favorite))
        .execute(conn)?;

    expect_one(result)
}

pub fn get_feeds_to_refresh(
    conn: &MysqlConnection,
    favorites_not_updated_in: std::time::Duration,
    others_not_updated_in: std::time::Duration,
    max_results: i64,
) -> Result<Vec<Feed>, Error> {
    use chrono::Duration;
    use crate::schema::feed::dsl::{is_favorite, last_fetch};

    let now = chrono::Utc::now().naive_utc();

    let fav_cutoff = now - Duration::from_std(favorites_not_updated_in)?;
    let oth_cutoff = now - Duration::from_std(others_not_updated_in)?;

    Ok(feed::feed
        .filter(
            last_fetch
                .is_null()
                .or(is_favorite.eq(true).and(last_fetch.lt(fav_cutoff)))
                .or(is_favorite.eq(false).and(last_fetch.lt(oth_cutoff))),
        )
        .select(feed::feed::all_columns())
        .limit(max_results)
        .load::<Feed>(conn)?)
}

pub fn update_fetched_feed(
    conn: &MysqlConnection,
    feed_id: u32,
    fetch_result: Result<FetchedFeed, Error>,
) -> Result<(), Error> {
    // Fighting the borrow-checker: fetch_result must be re-created.
    let fetch_result = match fetch_result {
        // Insert articles
        Ok(fetched_feed) => {
            let old_urls = article::article
                .filter(article::feed_id.eq(feed_id))
                .select(article::url)
                .load::<String>(conn)?;

            let is_new_article = |fetched_article: &&Article| {
                !old_urls
                    .iter()
                    .any(|old_url| &fetched_article.url == old_url)
            };

            let new_articles: Vec<&Article> = fetched_feed
                .articles
                .iter()
                .rev()
                .filter(is_new_article)
                .collect();

            let new_articles_count = new_articles.len();

            let inserted_count = insert_into(article::article)
                .values(new_articles)
                .execute(conn)?;

            if inserted_count != new_articles_count {
                return Err(err_msg(format!(
                    "Tried adding {} new article(s), but {} were inserted",
                    new_articles_count, inserted_count
                )));
            }

            Ok(fetched_feed)
        }
        Err(err) => Err(err),
    };

    // Update feed
    let (feed_name, err_msg) = match fetch_result {
        Ok(fetched_feed) => (Some(fetched_feed.name), None),
        Err(err) => (None, Some(format!("{}", err))),
    };

    let result = update(feed::feed.find(feed_id))
        .set((
            feed::last_fetch.eq(chrono::Utc::now().naive_utc()),
            feed::last_fetch_error.eq(err_msg),
        ))
        .execute(conn)?;

    expect_one(result)?;

    if let Some(feed_name) = feed_name {
        // If the name isn't set, set it now
        update(feed::feed.find(feed_id).filter(feed::name.eq(feed::url)))
            .set(feed::name.eq(feed_name))
            .execute(conn)?;
    }

    Ok(())
}

pub fn delete_feed(conn: &MysqlConnection, slug: &str, feed_id: u32) -> Result<(), Error> {
    let account_id = get_account_id_by_feed_id(conn, slug, feed_id)?;

    delete(article::article.filter(article::feed_id.eq(feed_id))).execute(conn)?;

    let result = delete(feed::feed.find(feed_id)).execute(conn)?;
    expect_one(result)?;

    // If this was the last feed, delete the account.
    let num_feeds = feed::feed
        .filter(feed::account_id.eq(account_id))
        .select(diesel::dsl::count_star())
        .first::<i64>(conn)?;

    if num_feeds == 0 {
        let result = delete(account::account.find(account_id)).execute(conn)?;
        expect_one(result)?;
    }

    Ok(())
}

pub fn get_article(
    conn: &MysqlConnection,
    slug: &str,
    feed_id: u32,
    article_id: u32,
) -> Result<Article, Error> {
    Ok(account::account
        .inner_join(feed::feed.inner_join(article::article))
        .filter(account::slug.eq(slug))
        .filter(feed::feed_id.eq(feed_id))
        .filter(article::article_id.eq(article_id))
        .select(article::article::all_columns())
        .first::<Article>(conn)?)
}

pub fn update_article(
    conn: &MysqlConnection,
    slug: &str,
    feed_id: u32,
    article: &Article,
) -> Result<(), Error> {
    get_article(conn, slug, feed_id, article.article_id)?;

    let result = update(article::article.find(article.article_id))
        .set(article::is_read.eq(article.is_read))
        .execute(conn)?;

    expect_one(result)
}

#[derive(Debug)]
pub struct GetArticlesParams {
    pub feed_id: Option<u32>,
    pub favorite_only: bool,
    pub unread_only: bool,
}

type BoxedBool<T, U> = Box<BoxableExpression<T, U, SqlType = Bool>>;

fn always_true<T>(column: T) -> Box<diesel::expression::operators::Eq<T, T>>
where
    T: ExpressionMethods + Copy,
{
    Box::new(column.eq(column))
}

pub fn get_articles(
    conn: &MysqlConnection,
    slug: &str,
    params: &GetArticlesParams,
) -> Result<Vec<Article>, Error> {
    let match_feed_id: BoxedBool<_, _> = if let Some(id) = params.feed_id {
        Box::new(feed::feed_id.eq(id))
    } else {
        always_true(feed::feed_id)
    };

    let match_favorite: BoxedBool<_, _> = if params.favorite_only {
        Box::new(feed::is_favorite.eq(true))
    } else {
        always_true(feed::feed_id)
    };

    let match_unread: BoxedBool<_, _> = if params.unread_only {
        Box::new(article::is_read.eq(false))
    } else {
        always_true(article::article_id)
    };

    Ok(account::account
        .inner_join(feed::feed.inner_join(article::article))
        .filter(account::slug.eq(slug))
        .filter(match_feed_id)
        .filter(match_favorite)
        .filter(match_unread)
        .order((
            feed::is_favorite.desc(),
            feed::name.asc(),
            article::created.desc(),
            article::article_id.desc(),
        ))
        .select(article::article::all_columns())
        .load::<Article>(conn)?)
}
