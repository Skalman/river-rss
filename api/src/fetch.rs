use crate::models;
use failure::{err_msg, Error};
use std::default::Default;
use std::io::BufReader;

pub struct FetchedFeed {
    pub name: String,
    pub articles: Vec<models::Article>,
}

pub fn fetch(feed: &models::Feed) -> Result<FetchedFeed, Error> {
    let client = reqwest::Client::new();
    let text = get_text(&client, &feed.url)?;

    parse_atom(&text)
        .or_else(|_| parse_rss(&text))
        .map(|mut fetched_feed| {
            for article in fetched_feed.articles.iter_mut() {
                article.feed_id = feed.feed_id;
            }

            fetched_feed
        })
        .map_err(|_| err_msg("Failed to parse as Atom or RSS"))
}

fn parse_rss(text: &str) -> Result<FetchedFeed, Error> {
    let feed = rss::Channel::read_from(BufReader::new(text.as_bytes()))?;

    let articles = feed
        .items()
        .iter()
        .filter_map(convert_rss_article)
        .collect();

    Ok(FetchedFeed {
        name: feed.title().to_string(),
        articles,
    })
}

fn convert_rss_article(article: &rss::Item) -> Option<models::Article> {
    let title = article.title()?;
    let published = article.pub_date().and_then(parse_date);
    let url = article.link()?;
    let download_url = article.enclosure().map(|enc| enc.url().to_string());

    Some(models::Article {
        article_id: Default::default(),
        feed_id: Default::default(),
        title: title.to_string(),
        url: url.to_string(),
        download_url,
        is_read: false,
        published,
        created: chrono::Utc::now().naive_utc(),
    })
}

fn parse_date(string: &str) -> Option<chrono::NaiveDateTime> {
    // Atom seems to use RFC 3339, and RSS RFC 2822
    chrono::DateTime::parse_from_rfc3339(string)
        .or_else(|_| chrono::DateTime::parse_from_rfc2822(string))
        .map(|parsed| parsed.naive_utc())
        .map_err(|err| {
            eprintln!("Debug: date parse error for '{}' {:?}", string, err);
        })
        .ok()
}

fn convert_atom_article(article: &atom_syndication::Entry) -> Option<models::Article> {
    let published = article.published().and_then(parse_date);

    let mut url = None;
    let mut download_url = None;

    for link in article.links() {
        if url == None {
            url = Some(link.href().to_string());
        }

        match link.rel() {
            "alternate" => {
                url = Some(link.href().to_string());
            }
            "enclosure" => {
                download_url = Some(link.href().to_string());
            }
            _ => {}
        }
    }

    let url = url?;

    Some(models::Article {
        article_id: Default::default(),
        feed_id: Default::default(),
        title: article.title().to_string(),
        url,
        download_url,
        is_read: false,
        published,
        created: chrono::Utc::now().naive_utc(),
    })
}

fn parse_atom(text: &str) -> Result<FetchedFeed, Error> {
    let feed = text.parse::<atom_syndication::Feed>()?;

    let articles = feed
        .entries()
        .iter()
        .filter_map(convert_atom_article)
        .collect();

    Ok(FetchedFeed {
        name: feed.title().to_string(),
        articles,
    })
}

fn get_text(client: &reqwest::Client, url: &str) -> Result<String, Error> {
    Ok(client.get(url).send()?.text()?)
}
