use failure::Error;
use futures::Future;
use futures::Stream;
use gotham::handler::HandlerError;
use gotham::http::response::create_response;
use gotham::state::{FromState, State};
use hyper::mime;
use std::env;

pub fn create_json_response<T, E>(result: Result<T, E>, state: State) -> (State, hyper::Response)
where
    T: serde::Serialize,
    E: std::fmt::Display + Sized,
{
    let orig_is_ok = result.is_ok();
    let mut is_ok = orig_is_ok;

    let result = match result {
        Ok(content) => serde_json::to_vec(&json!(content)),
        Err(err) => serde_json::to_vec(&json!({
            "error": true,
            "message": format!("{}", err),
        })),
    };

    let content = match result {
        Ok(content) => content,
        Err(json_err) => {
            let what = if orig_is_ok { "content" } else { "error" };
            is_ok = false;
            serde_json::to_vec(&json!({
                "error": true,
                "message": format!("Failed to serialize {} as JSON", what),
                "json_error": true,
                "json_error_message": format!("{:?}", json_err),
            }))
            .unwrap_or_else(|_| {
                br#"{"error":true,"message":"JSON serialization error","json_error":true}"#.to_vec()
            })
        }
    };

    let status_code = if is_ok {
        hyper::StatusCode::Ok
    } else {
        hyper::StatusCode::InternalServerError
    };

    let response = create_response(&state, status_code, Some((content, mime::APPLICATION_JSON)));

    let response = response.with_header(hyper::header::AccessControlAllowOrigin::Value(
        env::var("CORS_ALLOW").unwrap(),
    ));

    (state, response)
}

pub fn json_body_handler<TModel, F>(
    mut state: State,
    callback: F,
) -> Box<Future<Item = (State, hyper::Response), Error = (State, HandlerError)>>
where
    TModel: serde::de::DeserializeOwned + std::any::Any,
    F: 'static
        + Sized
        + FnOnce(State, Result<TModel, Error>) -> Result<(State, hyper::Response), (State, Error)>,
{
    let body = hyper::Body::take_from(&mut state);
    let f: Box<Future<Item = (State, hyper::Response), Error = (State, HandlerError)>> = Box::new(
        body.concat2()
            .then(|res| {
                let body = res?;
                let json = String::from_utf8(body.to_vec())?;
                let model = serde_json::from_str::<TModel>(&json)?;
                Ok(model) as Result<TModel, Error>
            })
            .then(|res| {
                let res = callback(state, res);

                match res {
                    Ok(response) => Ok(response),
                    Err((state, err)) => Ok(create_json_response(Err(err) as Result<(), _>, state)),
                }
            }),
    );

    f
}

pub fn create_options_response(
    state: State,
    allowed_methods: Vec<hyper::Method>,
) -> (State, hyper::Response) {
    let response = create_response(&state, hyper::StatusCode::Ok, None)
        .with_header(hyper::header::AccessControlAllowOrigin::Value(
            env::var("CORS_ALLOW").unwrap(),
        ))
        .with_header(hyper::header::AccessControlAllowMethods(allowed_methods))
        .with_header(hyper::header::AccessControlMaxAge(86400))
        .with_header(hyper::header::AccessControlAllowHeaders(vec![
            unicase::Ascii::new("Content-Type".to_owned()),
        ]));

    (state, response)
}
