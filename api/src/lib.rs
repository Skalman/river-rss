// Temporarily, before the release of the version after Diesel 1.3.0
#![allow(proc_macro_derive_resolution_fallback)]

#[macro_use]
extern crate diesel;
extern crate dotenv;

extern crate serde;
#[macro_use]
extern crate serde_derive;

mod data_connection;
mod models;
pub mod schema;
pub use crate::data_connection::establish_connection;
