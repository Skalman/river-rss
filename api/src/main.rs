// Temporarily, before the release of the version after Diesel 1.3.0
#![allow(proc_macro_derive_resolution_fallback)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate gotham_derive;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

mod data;
mod data_connection;
mod fetch;
mod gotham_helpers;
pub mod models;
pub mod schema;
mod update_job;

use crate::data::{
    delete_feed, get_account, get_article, get_articles, get_feed, get_feeds, insert_feed,
    update_article, update_feed, GetArticlesParams,
};
use crate::data_connection::establish_connection as conn;
use crate::gotham_helpers::{create_json_response, create_options_response, json_body_handler};
use crate::update_job::refresh_feed;
use dotenv::dotenv;
use failure::err_msg;
use gotham::http::response::create_response;
use gotham::router::builder::build_simple_router;
use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{FromState, State};
use hyper::mime;
use std::env;
use std::thread;

fn main() {
    dotenv().ok();

    let api_host = env::var("API_HOST").expect("API_HOST must be set");

    thread::spawn(update_job::start);

    println!("Listening for requests at http://{}", api_host);
    gotham::start(api_host, router());
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct SlugPathExtractor {
    slug: String,
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct FeedPathExtractor {
    slug: String,
    feed_id: u32,
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct ArticlePathExtractor {
    slug: String,
    feed_id: u32,
    article_id: u32,
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct ArticleSearchParamsExtractor {
    favorite_only: Option<bool>,
    unread_only: Option<bool>,
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct FeedArticleSearchParamsExtractor {
    unread_only: Option<bool>,
}

#[derive(Deserialize, StateData, StaticResponseExtender, Debug)]
struct UrlParamsExtractor {
    url: String,
}

fn router() -> Router {
    build_simple_router(|route| {
        route.get("/").to(|state| {
            let res = create_response(
                &state,
                hyper::StatusCode::Ok,
                Some((b"Hello, world!".to_vec(), mime::TEXT_PLAIN)),
            );

            (state, res)
        });

        route.scope("/api", |route| {
            route.associate("/:slug/account", |assoc| {
                let mut assoc = assoc.with_path_extractor::<SlugPathExtractor>();

                assoc.get().to(|state: State| {
                    let slug = &SlugPathExtractor::borrow_from(&state).slug;
                    create_json_response(get_account(&conn(), slug), state)
                });
            });

            route.associate("/:slug/feeds", |assoc| {
                let mut assoc = assoc.with_path_extractor::<SlugPathExtractor>();

                assoc.get().to(|state| {
                    let slug = &SlugPathExtractor::borrow_from(&state).slug;
                    create_json_response(get_feeds(&conn(), slug), state)
                });

                assoc
                    .options()
                    .to(|state| create_options_response(state, vec![hyper::Method::Post]));

                assoc
                    .post()
                    .with_query_string_extractor::<UrlParamsExtractor>()
                    .to(|state| {
                        let conn = conn();
                        let slug = &SlugPathExtractor::borrow_from(&state).slug;
                        let url = &UrlParamsExtractor::borrow_from(&state).url;

                        create_json_response(insert_feed(&conn, &slug, url), state)
                    });
            });

            route.associate("/:slug/feeds/articles", |assoc| {
                let mut assoc = assoc.with_path_extractor::<SlugPathExtractor>();
                let mut assoc = assoc.with_query_string_extractor::<ArticleSearchParamsExtractor>();

                assoc.get().to(|state| {
                    let path = SlugPathExtractor::borrow_from(&state);
                    let search = ArticleSearchParamsExtractor::borrow_from(&state);

                    create_json_response(
                        get_articles(
                            &conn(),
                            &path.slug,
                            &GetArticlesParams {
                                feed_id: None,
                                unread_only: search.unread_only.unwrap_or(true),
                                favorite_only: search.favorite_only.unwrap_or(true),
                            },
                        ),
                        state,
                    )
                });
            });

            route.associate("/:slug/feeds/:feed_id", |assoc| {
                let mut assoc = assoc.with_path_extractor::<FeedPathExtractor>();

                assoc.options().to(|state| {
                    create_options_response(state, vec![hyper::Method::Put, hyper::Method::Delete])
                });

                assoc.get().to(|state| {
                    let path = FeedPathExtractor::borrow_from(&state);
                    create_json_response(get_feed(&conn(), &path.slug, path.feed_id), state)
                });

                assoc.put().to(|state| {
                    json_body_handler::<models::Feed, _>(state, |state, res| match res {
                        Ok(feed) => {
                            let path = FeedPathExtractor::borrow_from(&state);
                            if path.feed_id != feed.feed_id {
                                return Err((state, err_msg("ID mismatch")));
                            }
                            let feed_result = update_feed(&conn(), &path.slug, &feed)
                                .and_then(|_| get_feed(&conn(), &path.slug, path.feed_id));

                            Ok(create_json_response(feed_result, state))
                        }
                        Err(err) => Err((state, err)),
                    })
                });

                assoc.delete().to(|state| {
                    let path = FeedPathExtractor::borrow_from(&state);

                    let result = delete_feed(&conn(), &path.slug, path.feed_id)
                        .and_then(|_| get_feeds(&conn(), &path.slug));

                    create_json_response(result, state)
                })
            });

            route.associate("/:slug/feeds/:feed_id/refresh", |assoc| {
                let mut assoc = assoc.with_path_extractor::<FeedPathExtractor>();

                assoc
                    .options()
                    .to(|state| create_options_response(state, vec![hyper::Method::Post]));

                assoc.post().to(|state| {
                    let path = FeedPathExtractor::borrow_from(&state);
                    let conn = conn();

                    let feed = get_feed(&conn, &path.slug, path.feed_id);
                    let feed = match feed {
                        Ok(feed) => feed,
                        Err(_) => {
                            return create_json_response(feed, state);
                        }
                    };

                    refresh_feed(&conn, &feed.feed);

                    let feed = get_feed(&conn, &path.slug, path.feed_id);
                    create_json_response(feed, state)
                });
            });

            route.associate("/:slug/feeds/:feed_id/articles", |assoc| {
                let mut assoc = assoc.with_path_extractor::<FeedPathExtractor>();
                let mut assoc =
                    assoc.with_query_string_extractor::<FeedArticleSearchParamsExtractor>();

                assoc.get().to(|state| {
                    let path = FeedPathExtractor::borrow_from(&state);
                    let search = FeedArticleSearchParamsExtractor::borrow_from(&state);
                    create_json_response(
                        get_articles(
                            &conn(),
                            &path.slug,
                            &GetArticlesParams {
                                feed_id: Some(path.feed_id),
                                unread_only: search.unread_only.unwrap_or(true),
                                favorite_only: false,
                            },
                        ),
                        state,
                    )
                });
            });

            route.associate("/:slug/feeds/:feed_id/articles/:article_id", |assoc| {
                let mut assoc = assoc.with_path_extractor::<ArticlePathExtractor>();

                assoc
                    .options()
                    .to(|state| create_options_response(state, vec![hyper::Method::Put]));

                assoc.put().to(|state| {
                    json_body_handler::<models::Article, _>(state, |state, res| match res {
                        Ok(article) => {
                            let path = ArticlePathExtractor::borrow_from(&state);
                            if path.article_id != article.article_id {
                                return Err((state, err_msg("ID mismatch")));
                            }
                            let article_result =
                                update_article(&conn(), &path.slug, path.feed_id, &article)
                                    .and_then(|_| {
                                        get_article(
                                            &conn(),
                                            &path.slug,
                                            path.feed_id,
                                            path.article_id,
                                        )
                                    });

                            Ok(create_json_response(article_result, state))
                        }
                        Err(err) => Err((state, err)),
                    })
                })
            });
        });
    })
}
