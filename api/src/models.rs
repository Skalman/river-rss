use chrono::NaiveDateTime;
use crate::schema::*;

#[table_name = "account"]
#[derive(Queryable, Insertable, PartialEq, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Account {
    pub account_id: u32,
    pub slug: String,
    pub created: NaiveDateTime,
}

#[table_name = "feed"]
#[derive(Queryable, Insertable, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Feed {
    pub feed_id: u32,
    pub account_id: u32,
    pub url: String,
    pub name: String,
    pub is_favorite: bool,
    pub last_fetch: Option<NaiveDateTime>,
    pub last_fetch_error: Option<String>,
    pub created: NaiveDateTime,
}

#[table_name = "article"]
#[derive(Queryable, Insertable, PartialEq, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Article {
    pub article_id: u32,
    pub feed_id: u32,
    pub title: String,
    pub url: String,
    pub download_url: Option<String>,
    pub is_read: bool,
    pub published: Option<NaiveDateTime>,
    pub created: NaiveDateTime,
}
