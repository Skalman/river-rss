table! {
    account (account_id) {
        account_id -> Unsigned<Integer>,
        slug -> Varchar,
        created -> Timestamp,
    }
}

table! {
    article (article_id) {
        article_id -> Unsigned<Integer>,
        feed_id -> Unsigned<Integer>,
        title -> Varchar,
        url -> Varchar,
        download_url -> Nullable<Varchar>,
        is_read -> Bool,
        published -> Nullable<Timestamp>,
        created -> Timestamp,
    }
}

table! {
    feed (feed_id) {
        feed_id -> Unsigned<Integer>,
        account_id -> Unsigned<Integer>,
        url -> Varchar,
        name -> Varchar,
        is_favorite -> Bool,
        last_fetch -> Nullable<Timestamp>,
        last_fetch_error -> Nullable<Varchar>,
        created -> Timestamp,
    }
}

joinable!(article -> feed (feed_id));
joinable!(feed -> account (account_id));

allow_tables_to_appear_in_same_query!(account, article, feed);
