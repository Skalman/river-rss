use crate::data::{get_feeds_to_refresh, update_fetched_feed};
use crate::data_connection::establish_connection as conn;
use crate::fetch::fetch;
use crate::models::Feed;
use std::thread;
use std::time::Duration;

const UPDATE_FAVORITES_EVERY: Duration = Duration::from_secs(60 * 60 * 24);
const UPDATE_OTHERS_EVERY: Duration = Duration::from_secs(60 * 60 * 24 * 7);
const FEEDS_PER_BATCH: i64 = 20;

pub fn start() -> ! {
    println!("Running RSS jobs");
    loop {
        let feeds = get_feeds_to_refresh(
            &conn(),
            UPDATE_FAVORITES_EVERY,
            UPDATE_OTHERS_EVERY,
            FEEDS_PER_BATCH,
        );
        match feeds {
            Ok(feeds) => refresh_feeds(&feeds),
            Err(err) => eprintln!("{}", err),
        }
        thread::sleep(Duration::from_secs(60));
    }
}

fn refresh_feeds(feeds: &[Feed]) {
    println!("Update {} feed(s)", feeds.len());

    let conn = conn();

    for feed in feeds.iter() {
        refresh_feed(&conn, feed);
    }
}

pub fn refresh_feed(conn: &diesel::MysqlConnection, feed: &Feed) {
    let result = fetch(feed);

    match &result {
        Ok(fetched) => println!(
            "Got feed #{} '{}' with {} articles",
            feed.feed_id,
            feed.url,
            fetched.articles.len()
        ),
        Err(err) => eprintln!(
            "Failed to fetch feed #{} '{}': {}",
            feed.feed_id, feed.url, err
        ),
    }

    if let Err(err) = update_fetched_feed(&conn, feed.feed_id, result) {
        eprintln!("Failed to save feed update for #{}: {}", feed.feed_id, err);
    }
}
