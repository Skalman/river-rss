import * as React from "react";
import { DataService } from "../dataservice";
import { Store } from "../store";
import { FeedView } from "./feedview";

interface IAppProps {
  data: DataService;
}

interface IAppState {
  isEditingSlug: boolean;
  store?: Store;
}

export class App extends React.Component<IAppProps, IAppState> {
  state: IAppState = {
    isEditingSlug: false,
    store: undefined
  };

  private slugInputRef = React.createRef<HTMLInputElement>();
  private slugLinkRef = React.createRef<HTMLAnchorElement>();

  private normalizeSlug(slug?: string) {
    slug =
      slug &&
      slug
        .trim()
        .replace(/[_ -]+/g, "-")
        .toLowerCase();

    if (slug) {
      return slug;
    }
  }

  private getSlugFromUrl() {
    return this.normalizeSlug(location.pathname.replace(/^.*?\//, ""));
  }

  private setStoreFromUrl() {
    this.setStore(this.getSlugFromUrl());
  }

  private setStore(slug?: string) {
    slug = this.normalizeSlug(slug);

    if (slug && /[^a-z0-9-]/.test(slug)) {
      alert("Invalid slug (a-z, 0-9 and - allowed)");
      return;
    }

    this.setIsEditing(false);

    const curSlug = this.state.store && this.state.store.slug;
    if (slug === curSlug) {
      return;
    }

    const urlSlug = this.getSlugFromUrl();

    if (slug !== urlSlug) {
      history.pushState(
        undefined,
        slug ? `${slug} - RSS River` : "RSS River",
        slug || "./"
      );
    }

    this.setState({
      store: slug ? new Store(this.props.data, slug) : undefined
    });
  }

  componentDidMount() {
    this.setStoreFromUrl = this.setStoreFromUrl.bind(this);
    this.setStoreFromUrl();
    window.addEventListener("popstate", this.setStoreFromUrl);
  }

  componentWillUnmount() {
    window.removeEventListener("popstate", this.setStoreFromUrl);
  }

  private focusNext = false;
  componentDidUpdate() {
    if (!this.focusNext) {
      return;
    }

    this.focusNext = false;
    const elem = this.slugInputRef.current || this.slugLinkRef.current;
    if (elem) {
      elem.focus();
    }
  }

  private toggleIsEditing(e: { preventDefault: () => void }) {
    e.preventDefault();
    this.setIsEditing(!this.state.isEditingSlug);
  }

  private setIsEditing(value: boolean) {
    if (this.state.isEditingSlug !== value) {
      this.setState({ isEditingSlug: value });
      this.focusNext = true;
    }
  }

  render() {
    const { store, isEditingSlug } = this.state;
    const isEditing = !store || isEditingSlug;
    const slug = (store && store.slug) || "";

    return (
      <>
        <h1>
          <span className="logo" />
          Feeds
          <form
            className="text-muted float-right"
            onSubmit={e => {
              e.preventDefault();
              const elem = this.slugInputRef.current;
              this.setStore(elem ? elem.value : undefined);
            }}
          >
            <small>
              {!isEditing && (
                <a
                  href=""
                  ref={this.slugLinkRef}
                  onClick={e => {
                    this.toggleIsEditing(e);
                  }}
                >
                  {slug}
                </a>
              )}
              {isEditing && (
                <label>
                  Account:{" "}
                  <input
                    defaultValue={slug}
                    ref={this.slugInputRef}
                    onKeyDown={e => {
                      if (e.key === "Escape") {
                        this.toggleIsEditing(e);
                      }
                    }}
                  />
                </label>
              )}
            </small>
          </form>
        </h1>

        {store && <FeedView store={store} />}
      </>
    );
  }
}
