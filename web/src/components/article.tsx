import * as React from "react";
import { IArticle } from "../models";
import { Store } from "../store";

interface IArticleProps {
  store: Store;
  article: IArticle;
}

export function Article(props: IArticleProps) {
  const { store, article } = props;

  return (
    <li className={article.isRead ? "text-muted is-read" : undefined}>
      <button
        className={"mark-as-read"}
        type="button"
        onClick={() => store.setArticleIsRead(article, !article.isRead)}
      />
      <a href={article.url}>{article.title}</a>{" "}
      {article.downloadUrl && (
        <a href={article.downloadUrl} download className="download-link">
          download
        </a>
      )}
    </li>
  );
}
