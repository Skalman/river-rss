import * as React from "react";
import { IFeed } from "../models";
import { Store } from "../store";
import { Article } from "./article";

interface IArticleListProps {
  store: Store;
  feed: IFeed;
}

export function ArticleList({ store, feed }: IArticleListProps) {
  let articles = store.getArticles(feed);
  if (!articles) {
    return <div>No articles loaded</div>;
  }

  if (store.showUnreadOnly) {
    articles = articles.filter(
      x => !x.isRead || store.isArticleRecentlyRead(x)
    );
  }

  return (
    <ul className="list-unstyled">
      {articles.map(article => (
        <Article key={article.articleId} store={store} article={article} />
      ))}
    </ul>
  );
}
