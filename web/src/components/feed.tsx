import * as React from "react";
import { IFeed } from "../models";
import { Store } from "../store";
import { relativeDate } from "../util";
import { ArticleList } from "./articlelist";

interface IFeedProps {
  store: Store;
  feed: IFeed;
}

interface IFeedState {
  isExtendedMenuOpen: boolean;
}

export class Feed extends React.Component<IFeedProps, IFeedState> {
  state: IFeedState = {
    isExtendedMenuOpen: false
  };

  render() {
    const { feed, store } = this.props;
    const { showUnreadOnly } = store;

    if (store.isFeedRemoved(feed)) {
      return (
        <div className="d-flex">
          <span className="flex-grow-1">
            <small>[Removed]</small> <del>{feed.feed.name}</del>{" "}
          </span>
          <a
            href="#"
            onClick={e => {
              e.preventDefault();
              store.undoRemoveFeed(feed);
            }}
          >
            Undo
          </a>
        </div>
      );
    }

    return (
      <details open={feed.feed.isFavorite}>
        <summary
          className="d-flex"
          title={feed.feed.lastFetchError || undefined}
          onClick={async e => {
            e.preventDefault();
            const isFavorite = !feed.feed.isFavorite;
            store.setFeedFavorite(feed.feed, isFavorite);

            if (isFavorite) {
              await store.fetchFeedArticles(feed.feed, showUnreadOnly);
            }
          }}
        >
          <span className="flex-grow-1">
            {feed.feed.lastFetchError && (
              <span className="text-warning">⚠ </span>
            )}
            {feed.feed.name} ({feed.meta.numUnreadArticles} unread)
          </span>
          <ul className="text-muted list-inline">
            <li>{feed.meta.numArticles} total</li>
            <li>
              {store.isFeedRefreshing(feed) && "Refreshing..."}
              {!store.isFeedRefreshing(feed) && (
                <a
                  href="#"
                  title={
                    feed.feed.lastFetch
                      ? `Last fetched ${feed.feed.lastFetch}`
                      : "Refresh now"
                  }
                  onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    store.refreshFeed(feed);
                  }}
                >
                  {!feed.feed.lastFetch && "never fetched"}
                  {feed.feed.lastFetch && (
                    <time dateTime={feed.feed.lastFetch}>
                      {relativeDate(feed.feed.lastFetch)}
                    </time>
                  )}
                </a>
              )}
            </li>
            <li
              className={
                "menu-ext " + (this.state.isExtendedMenuOpen ? "open" : "")
              }
              onClick={e => e.stopPropagation()}
              onMouseEnter={() => this.setState({ isExtendedMenuOpen: true })}
              onMouseLeave={() => this.setState({ isExtendedMenuOpen: false })}
            >
              <a
                href="#"
                onClick={e => {
                  e.preventDefault();
                  this.setState({
                    isExtendedMenuOpen: !this.state.isExtendedMenuOpen
                  });
                }}
              >
                ...
              </a>
              <ul>
                <li>
                  <a href={feed.feed.url} onClick={e => e.stopPropagation()}>
                    Feed URL
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-danger"
                    onClick={async e => {
                      e.preventDefault();
                      await store.removeFeed(feed);
                    }}
                  >
                    Unsubscribe
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </summary>
        <ArticleList store={store} feed={feed} />
      </details>
    );
  }
}
