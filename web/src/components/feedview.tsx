import * as React from "react";
import { Store } from "../store";
import { Feed } from "./feed";

interface IFeedViewProps {
  store: Store;
}

export class FeedView extends React.Component<IFeedViewProps> {
  private timer?: ReturnType<typeof setInterval>;

  private async update() {
    const { store } = this.props;
    await store.updateFeeds();
    await store.fetchArticles(store.showUnreadOnly);
  }

  componentDidMount() {
    this.update();
    this.timer = setInterval(() => this.update(), 30_000);

    this.props.store.onChange(() => this.forceUpdate());
  }

  componentWillUnmount() {
    clearInterval(this.timer!);
  }

  componentDidUpdate(prevProps: IFeedViewProps) {
    if (prevProps.store !== this.props.store) {
      this.update();
      this.props.store.onChange(() => this.forceUpdate());
    }
  }

  render() {
    const { store } = this.props;
    const { showUnreadOnly } = store;

    let feeds = store.feeds;

    if (feeds && showUnreadOnly) {
      feeds = feeds.filter(
        x => x.meta.numUnreadArticles > 0 || store.isFeedRecentlyRead(x)
      );
    }

    return (
      <>
        <p>
          <label>
            <input
              type="checkbox"
              checked={showUnreadOnly}
              onChange={async () => {
                const newShowUnreadOnly = !showUnreadOnly;
                store.setShowUnreadOnly(newShowUnreadOnly);
                if (!newShowUnreadOnly) {
                  await store.fetchArticles(newShowUnreadOnly);
                }
              }}
            />
            Show unread only
          </label>{" "}
          <button
            type="button"
            onClick={async () => {
              const url = prompt("Enter a feed URL");
              if (url) {
                await store.addFeed(url);
              }
            }}
          >
            Subscribe
          </button>{" "}
          <a
            href="#"
            onClick={e => {
              e.preventDefault();
              store.toggleTheme();
            }}
            className="swatch"
          >
            Switch theme
          </a>
        </p>
        {!feeds ? (
          <p>No feeds loaded</p>
        ) : (
          <ul className="list-unstyled">
            {feeds.map(feed => (
              <li key={feed.feed.feedId}>
                <Feed store={store} feed={feed} />
              </li>
            ))}
          </ul>
        )}
        <div />
      </>
    );
  }
}
