import { DataServiceBase } from "./dataservicebase";
import { IArticle, IFeed, IFeedData } from "./models";

export class DataService extends DataServiceBase {
  constructor(apiHost: string) {
    super(`//${apiHost}/api/`);
  }

  getFeeds(args: { slug: string }) {
    return this.get<IFeed[]>(":slug/feeds", args);
  }

  getArticles(args: {
    slug: string;
    favoriteOnly?: boolean;
    unreadOnly?: boolean;
  }) {
    return this.get<IArticle[]>(":slug/feeds/articles", args);
  }

  putFeed(args: { slug: string; feed: IFeedData }) {
    return this.put<IFeed>(":slug/feeds/:feed_id", args.feed, {
      slug: args.slug,
      feedId: args.feed.feedId
    });
  }

  putArticle(args: { slug: string; article: IArticle }) {
    return this.put<IArticle>(
      ":slug/feeds/:feed_id/articles/:article_id",
      args.article,
      {
        slug: args.slug,
        feedId: args.article.feedId,
        articleId: args.article.articleId
      }
    );
  }

  getFeedArticles(args: {
    slug: string;
    feedId: number;
    unreadOnly?: boolean;
  }) {
    return this.get<IArticle[]>(":slug/feeds/:feed_id/articles", args);
  }

  refreshFeed(args: { slug: string; feedId: number }) {
    return this.post<IFeed>(":slug/feeds/:feed_id/refresh", undefined, args);
  }

  postFeed(args: { slug: string; url: string }) {
    return this.post<IFeed>(":slug/feeds", undefined, args);
  }

  deleteFeed(args: { slug: string; feedId: number }) {
    return this.delete<IFeed[]>(":slug/feeds/:feed_id", args);
  }
}
