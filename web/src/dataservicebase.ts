export class DataServiceBase {
  constructor(private readonly urlPrefix: string) {}

  private makeUrl(url: string, args: IUrlArgs = {}) {
    url = url.replace(/(^|\/):([^/]+)/g, (match, p1, pathPart) => {
      pathPart = toCamelCase(pathPart);
      if (pathPart in args) {
        const val = args[pathPart];
        if (val !== undefined) {
          args[pathPart] = undefined;
          return p1 + encode(val);
        }
      }

      throw new Error(
        `Expected arg '${pathPart}', because it is in the URL '${url}' (found ${Object.keys(
          args
        ).join(", ")})`
      );
    });

    const queryString = Object.entries(args)
      .filter(([, val]) => val !== undefined)
      .map(([key, val]) => `${toSnakeCase(encode(key))}=${encode(val!)}`)
      .join("&");

    if (queryString) {
      const sep = url.includes("?") ? "&" : "?";
      url += sep + queryString;
    }

    return this.urlPrefix + url;
  }

  private async makeRequest<T>(
    method: string,
    url: string,
    args?: IUrlArgs,
    body?: object
  ) {
    const response = await fetch(this.makeUrl(url, args), {
      method,
      body: body && JSON.stringify(body),
      headers: body && {
        "Content-Type": "application/json"
      }
    });

    if (response.ok) {
      const data: Promise<T> = response.json();
      return data;
    } else {
      const data: IApiErrorResponse = await response.json();
      throw new ApiError(url, data);
    }
  }

  protected async get<T>(url: string, args?: IUrlArgs) {
    return this.makeRequest<T>("GET", url, args);
  }

  protected async post<T>(url: string, body?: object, args?: IUrlArgs) {
    return this.makeRequest<T>("POST", url, args, body);
  }

  protected async put<T>(url: string, body: object, args?: IUrlArgs) {
    return this.makeRequest<T>("PUT", url, args, body);
  }

  protected async delete<T>(url: string, args?: IUrlArgs) {
    return this.makeRequest<T>("DELETE", url, args);
  }
}

interface IUrlArgs {
  [key: string]: string | number | boolean | undefined;
}

interface IApiErrorResponse {
  error: true;
  message: string;
}

class ApiError extends Error {
  constructor(readonly url: string, readonly inner?: IApiErrorResponse) {
    super((inner && inner.message) || `Error when fetching ${url}`);
  }
}

function encode(arg: string | number | boolean) {
  return encodeURIComponent("" + arg);
}

function toSnakeCase(value: string) {
  return value.replace(/[A-Z]/g, letter => "_" + letter.toLowerCase());
}

function toCamelCase(value: string) {
  return value.replace(/_([a-z])/g, (_, letter) => letter.toUpperCase());
}
