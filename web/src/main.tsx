import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./components/app";
import { DataService } from "./dataservice";
import "./patched-typings";

const apiHost = process.env.API_HOST;
if (!apiHost) {
  throw new Error("Env var API_HOST is required");
}

const dataService = new DataService(apiHost);

const container = document.getElementById("app") || document.body;

function renderApp() {
  ReactDOM.render(<App data={dataService} />, container);
}

renderApp();

if (module.hot) {
  module.hot.accept(renderApp);
}
