export interface IAccount {
  accountId: number;
  slug: string;
  created: string;
}

export interface IFeedData {
  feedId: number;
  accountId: number;
  url: string;
  name: string;
  isFavorite: boolean;
  lastFetch: null | string;
  lastFetchError: null | string;
  created: string;
}

export interface IFeed {
  feed: IFeedData;
  meta: {
    numArticles: number;
    numUnreadArticles: number;
  };
}

export interface IArticle {
  articleId: number;
  feedId: number;
  title: string;
  url: string;
  downloadUrl: null | string;
  isRead: boolean;
  published: null | string;
  created: string;
}
