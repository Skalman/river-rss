export {};

// Fix typings declared in @typings/node
declare global {
  // tslint:disable-next-line:interface-name
  export interface NodeModule {
    hot?: {
      accept(callback: () => void): void;
    };
  }

  export var module: NodeModule;
}
