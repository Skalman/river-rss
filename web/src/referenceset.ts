export class ReferenceSet {
  // Use {} for a truthy value, whose identity can be checked
  private store: {
    [id: number]: {} | undefined;
  } = {};

  set(id: number, reference: {}) {
    this.store[id] = reference;
  }

  unset(id: number, reference: {}) {
    if (this.store[id] === reference) {
      this.store[id] = undefined;
      return true;
    }

    return false;
  }

  forceUnset(id: number) {
    this.store[id] = undefined;
  }

  isSet(id: number): boolean;
  // tslint:disable-next-line:unified-signatures Disallow `undefined` as second parameter
  isSet(id: number, reference: {}): boolean;
  isSet(id: number, reference?: {}) {
    if (reference) {
      return this.store[id] === reference;
    } else {
      return !!this.store[id];
    }
  }
}
