import { DataService } from "./dataservice";
import { IArticle, IFeed, IFeedData } from "./models";
import { ReferenceSet } from "./referenceset";
import { delay } from "./util";

interface IArticlesByFeedId {
  [feedId: number]: IArticle[] | undefined;
}

export class Store {
  constructor(private readonly data: DataService, readonly slug: string) {}

  showUnreadOnly = false;
  theme: "dark" | "light" = "dark";

  feeds?: IFeed[];
  isLoadingFeeds = false;
  private articlesByFeedId: IArticlesByFeedId = {};
  private feedsRecentlyMarkedAsRead = new ReferenceSet();
  private articlesRecentlyMarkedAsRead = new ReferenceSet();
  private feedsRefreshing = new ReferenceSet();
  private feedsRemoved = new ReferenceSet();

  private callbacks: (() => void)[] = [];
  onChange(callback: () => void) {
    this.callbacks.push(callback);
  }

  private triggerChange() {
    this.callbacks.forEach(x => x());
  }

  getArticles(feed: IFeed) {
    return this.articlesByFeedId[feed.feed.feedId];
  }

  isFeedRecentlyRead(feed: IFeed) {
    return this.feedsRecentlyMarkedAsRead.isSet(feed.feed.feedId);
  }

  isArticleRecentlyRead(article: IArticle) {
    return this.articlesRecentlyMarkedAsRead.isSet(article.articleId);
  }

  isFeedRefreshing(feed: IFeed) {
    return this.feedsRefreshing.isSet(feed.feed.feedId);
  }

  isFeedRemoved(feed: IFeed) {
    return this.feedsRemoved.isSet(feed.feed.feedId);
  }

  setShowUnreadOnly(showUnread: boolean) {
    this.showUnreadOnly = showUnread;
    // TODO Persist setting
    this.triggerChange();
  }

  toggleTheme() {
    this.theme = this.theme === "dark" ? "light" : "dark";
    if (this.theme === "dark") {
      document.documentElement.classList.remove("theme-light");
    } else {
      document.documentElement.classList.add("theme-light");
    }
    // TODO Persist setting
  }

  async updateFeeds() {
    const feeds = await this.data.getFeeds({ slug: this.slug });
    this.feeds = feeds;
    this.triggerChange();
  }

  async setFeedFavorite(feed: IFeedData, isFavorite: boolean) {
    feed.isFavorite = isFavorite;
    this.triggerChange();

    await this.data.putFeed({ slug: this.slug, feed });
    this.triggerChange();
  }

  async fetchArticles(unreadOnly: boolean) {
    const articles = await this.data.getArticles({
      slug: this.slug,
      favoriteOnly: true,
      unreadOnly
    });
    if (this.feeds) {
      for (const feed of this.feeds.filter(x => x.feed.isFavorite)) {
        this.articlesByFeedId[feed.feed.feedId] = [];
      }
    }
    this.patchArticles(articles);
    this.triggerChange();
  }

  async fetchFeedArticles(feed: IFeedData, unreadOnly: boolean) {
    const articles = await this.data.getFeedArticles({
      slug: this.slug,
      feedId: feed.feedId,
      unreadOnly
    });
    this.articlesByFeedId[feed.feedId] = [];
    this.patchArticles(articles);
    this.triggerChange();
  }

  async setArticleIsRead(article: IArticle, isRead: boolean) {
    const unreadDiff =
      isRead && !article.isRead ? -1 : !isRead && article.isRead ? 1 : 0;

    const feed =
      this.feeds && this.feeds.find(x => x.feed.feedId === article.feedId);

    if (feed) {
      feed.meta.numUnreadArticles += unreadDiff;
    }

    if (unreadDiff === -1) {
      const obj = {};
      this.feedsRecentlyMarkedAsRead.set(article.feedId, obj);
      this.articlesRecentlyMarkedAsRead.set(article.articleId, obj);
      setTimeout(() => {
        this.feedsRecentlyMarkedAsRead.unset(article.feedId, obj);
        this.articlesRecentlyMarkedAsRead.unset(article.articleId, obj);
        this.triggerChange();
      }, 5_000);
    }

    article.isRead = isRead;
    this.triggerChange();

    await this.data.putArticle({ slug: this.slug, article });
    this.triggerChange();
  }

  private async patchArticles(articles: IArticle[]) {
    const byFeedId = articles.reduce<IArticlesByFeedId>((obj, article) => {
      const arr = obj[article.feedId] || (obj[article.feedId] = []);
      arr.push(article);
      return obj;
    }, {});

    Object.assign(this.articlesByFeedId, byFeedId);
  }

  async refreshFeed(feed: IFeed): Promise<void> {
    const feedId = feed.feed.feedId;
    if (this.feedsRefreshing.isSet(feedId)) {
      return;
    }

    const obj = {};
    this.feedsRefreshing.set(feedId, obj);
    this.triggerChange();

    let didUpdate = false;
    try {
      const updatedFeed = await this.data.refreshFeed({
        slug: this.slug,
        feedId
      });

      if (this.feeds) {
        this.feeds = this.feeds.map(
          x => (x.feed.feedId === feedId ? updatedFeed : x)
        );
      }
      didUpdate = true;
    } catch (e) {
      console.error(e);
    }

    this.feedsRefreshing.unset(feedId, obj);
    this.triggerChange();

    if (didUpdate) {
      this.fetchFeedArticles(feed.feed, this.showUnreadOnly);
    }
  }

  async addFeed(url: string) {
    const feed = await this.data.postFeed({ slug: this.slug, url });
    if (this.feeds) {
      this.feeds.unshift(feed);
      this.triggerChange();
    }

    await this.refreshFeed(feed);
  }

  async removeFeed(feed: IFeed) {
    const obj = {};
    this.feedsRemoved.set(feed.feed.feedId, obj);
    this.triggerChange();

    await delay(5_000);

    if (this.feedsRemoved.unset(feed.feed.feedId, obj)) {
      await this.data.deleteFeed({
        slug: this.slug,
        feedId: feed.feed.feedId
      });
      if (this.feeds) {
        this.feeds = this.feeds.filter(x => x !== feed);
        this.triggerChange();
      }
    }
  }

  undoRemoveFeed(feed: IFeed) {
    this.feedsRemoved.forceUnset(feed.feed.feedId);
    this.triggerChange();
  }
}
