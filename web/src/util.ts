export function delay(timeout: number) {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

export function relativeDate(utcDateString: string) {
  if (!utcDateString.endsWith("Z")) {
    utcDateString += "Z";
  }

  const date = Date.parse(utcDateString);
  const now = Date.now();
  const duration = durationToString(Math.abs(date - now));

  if (date < now) {
    return `${duration} ago`;
  } else {
    return `in ${duration}`;
  }
}

function durationToString(duration: number) {
  const r = Math.round;

  duration /= 1000 * 60;
  if (duration < 0.5) {
    return "a few seconds";
  } else if (duration < 2) {
    return "a minute";
  } else if (duration < 60) {
    return `${r(duration)} minutes`;
  }

  duration /= 60;
  if (duration < 2) {
    return "an hour";
  } else if (duration < 24) {
    return `${r(duration)} hours`;
  }

  duration /= 24;
  if (duration < 2) {
    return "a day";
  } else if (duration < 11) {
    return `${r(duration)} days`;
  } else if (duration < 30) {
    return `${r(duration / 7)} weeks`;
  } else if (duration < 45) {
    return "a month";
  } else if (duration < 365) {
    return `${r(duration / 30)} months`;
  } else if (duration < 365 * 1.5) {
    return "a year";
  } else {
    return `${r(duration / 365)} years`;
  }
}
